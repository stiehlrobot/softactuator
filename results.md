# Softrobotic silicon actuator experiment
Team members: Avner Peled, James at Aalto University Biofilia lab 
https://www.aalto.fi/en/biofilia

Conducted an experiment with heater wire cured inside a soft robotic silicon 
actuators made out of Ecoflex 30 and 50 type silicon. 

### Materials

- Ecoflex 30 Silicon - https://www.smooth-on.com/products/ecoflex-00-30/
- Ecoflex 50 Silicon - https://www.smooth-on.com/products/ecoflex-00-50/
- Nickel chromium heater wire 80:20 https://www.omega.com/en-us/wire-and-cable/heating-wire/p/NI80

### Experiment results

| actuator type | Trigger voltage and current | observations |
| ---      |  ------  |---------|
| ecoflex 30 block |  maxed at 9v 500mA   |no noticeable expansion|
| ecoflex 50 block |  9V 1.5A  |noticeably expanding irregularly, popping sounds, perhaps escaping ethanol vapour from structure |
| ecoflex 30 tube |  13.5 V 850 mA  |Noticeable change in shape. Structure lifting itself up from ground toward path of least resistance (?), in this case to the side where coil is close to surface|
| ecoflex 50 tube |  13.5V 1A  |Structure transformed itself up from ground plane in 4 minutes, returned back to original shape in 1 minute|

### Images

1. [Ecoflex 50 tube at t0](/images/softrobotic1.png)
2. [Ecoflex 50 tube at t150sec](/images/softrobotic1peak.png)
3. [Ecoflex 30 tube at t0](/images/softrobotic2.png)
4. [Ecoflex 50 tube at t60sec](/images/softrobotic2peak.png)
